package com.fomin.view;

import com.fomin.controller.Controller;
import java.util.*;

public class View {

    private Map<String, String> menu;
    private Map<String, Printable> methodMenu;
    private Controller controller = new Controller();
    private static Scanner scanner = new Scanner(System.in);
    private Locale locale;
    private ResourceBundle resourceBundle;

    private void setMenu() {
        menu = new LinkedHashMap<>();
        menu.put("1", resourceBundle.getString("1"));
        menu.put("2", resourceBundle.getString("2"));
        menu.put("3", resourceBundle.getString("3"));
        menu.put("4", resourceBundle.getString("4"));
        menu.put("5", resourceBundle.getString("5"));
        menu.put("6", resourceBundle.getString("6"));
        menu.put("7", resourceBundle.getString("7"));
        menu.put("8", resourceBundle.getString("8"));
        menu.put("q", resourceBundle.getString("q"));
    }

    public View() {
        locale = new Locale("uk");
        resourceBundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
        methodMenu = new LinkedHashMap<>();
        methodMenu.put("1", this::pingPong);
        methodMenu.put("2", this::fibonacci);
        methodMenu.put("3", this::sumFibonacci);
        methodMenu.put("4", this::getSleepThreads);
        methodMenu.put("5", this::getMethods);
        methodMenu.put("6", this::setEnglish);
        methodMenu.put("7", this::setUkrainian);
        methodMenu.put("8", this::setGerman);
    }

    public void showMenu() {
        String keyMenu;
        do {
            System.out.println("\nMENU:");
            for (String str : menu.values()) {
                System.out.println(str);
            }
            System.out.println("Please, select menu point.");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.toUpperCase().equals("Q"));
    }

    private void pingPong() {
        controller.printPingPong();
    }

    private void fibonacci() {
        controller.fibonacci();
    }

    private void sumFibonacci() {
        controller.sumFibonacci();
    }

    private void getMethods() {
        controller.invoke();
    }

    private void getSleepThreads() {
        controller.sleepThread();
    }

    private void setEnglish(){
        locale = new Locale("en");
        resourceBundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }

    private void setUkrainian(){
        locale = new Locale("uk");
        resourceBundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }

    private void setGerman(){
        locale = new Locale("de");
        resourceBundle = ResourceBundle.getBundle("menu", locale);
        setMenu();
    }
}
