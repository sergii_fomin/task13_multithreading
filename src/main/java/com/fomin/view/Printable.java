package com.fomin.view;

@FunctionalInterface
public interface Printable {

    void print();
}
