package com.fomin.model;

import java.util.concurrent.*;

public class PingPong {
    private static Object sync = new Object();

    public void playThread() {
        Thread thread1 = pingThread();
        Thread thread2 = pongThread();
        thread1.start();
        thread2.start();
        try {
            thread1.join();
            thread2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void playRunnable() {
        Executor exe1 = Executors.newSingleThreadExecutor();
        Executor exe2 = Executors.newSingleThreadExecutor();
        exe1.execute(pingRunnable());
        exe2.execute(pongRunnable());
    }

    private Thread pingThread() {
        return new Thread(() -> {
            synchronized (sync) {
                System.out.println("Start " + Thread.currentThread().getName());
                for (int i = 0; i < 20; i++) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Ping ");
                    sync.notify();
                }
                System.out.println("Finish " + Thread.currentThread().getName());
            }
        });

    }

    private Thread pongThread() {
        return new Thread(() -> {
            synchronized (sync) {
                System.out.println("Start " + Thread.currentThread().getName());
                for (int i = 0; i < 10; i++) {
                    sync.notify();
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Pong ");
                }
                System.out.println("Finish " + Thread.currentThread().getName());
            }
        });
    }

    private Runnable pingRunnable() {
        return () -> {
            synchronized (sync) {
                System.out.println("Start " + Thread.currentThread().getName());
                for (int i = 0; i < 10; i++) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Ping ");
                    sync.notify();
                }
                System.out.println("Finish " + Thread.currentThread().getName());
            }
        };
    }

    private Runnable pongRunnable() {
        return () -> {
            synchronized (sync) {
                System.out.println("Start " + Thread.currentThread().getName());
                for (int i = 0; i < 10; i++) {
                    sync.notify();
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("Pong");
                }
                System.out.println("Finish " + Thread.currentThread().getName());
            }
        };
    }
}
