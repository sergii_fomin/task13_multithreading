package com.fomin.model;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Sleep {

    int count;
    public Sleep(int count) {
        this.count = count;
    }

    public void createSleepingThreads() {
        ScheduledExecutorService exe = Executors.newScheduledThreadPool(this.count);
        for(int i = 0; i < this.count; i++) {
            exe.schedule(sleep(), 500, TimeUnit.MILLISECONDS);
        }
        exe.shutdown();
    }

    private Runnable sleep() {
        return () -> {
            int amountTime = new Random().nextInt(10);
            try {
                System.out.printf("%s sleeping\n", Thread.currentThread().getName());
                TimeUnit.SECONDS.sleep(amountTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.printf("%s slept %d seconds\n", Thread.currentThread().getName(), amountTime);
        };
    }
}
