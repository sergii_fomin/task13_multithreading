package com.fomin.model;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FibonacciCallable implements Callable {

    private int[] sequence;
    public FibonacciCallable(int n) {
        Fibonacci fibonacci = new Fibonacci(n);
        this.sequence = fibonacci.createSequence();
    }

    @Override
    public Object call() throws Exception {
        int sum = 0;
        for (int item : this.sequence) {
            sum = sum + item;
        }
        return sum;
    }

    public void getSum() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        System.out.println("Sum " + executorService.submit(this::call).get());
        executorService.shutdown();
    }
}
