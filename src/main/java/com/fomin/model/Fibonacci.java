package com.fomin.model;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Fibonacci {
    private int[] sequence;
    private int n;
    private volatile static int i = 0;
    private static Object sync = new Object();

    public Fibonacci(int n) {
        this.n = n;
        this.sequence = new int[n];
    }

    public int[] createSequence() {
        this.sequence[0] = 1;
        this.sequence[1] = 1;
        for (int i = 2; i < n; i++) {
            this.sequence[i] = this.sequence[i - 1] + this.sequence[i - 2];
        }
        return this.sequence;
    }

    private Thread createThread1() {
        return new Thread(() -> {
            synchronized (sync) {
                System.out.println("Start " + Thread.currentThread().getName());
                int[] sequence = this.createSequence();
                while (i != this.n) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(sequence[i] + " " + Thread.currentThread().getName());
                    i++;
                    sync.notify();
                }
            }
        });
    }

    private Thread createThread2() {
        return new Thread(() -> {
            synchronized (sync) {
                System.out.println("Start " + Thread.currentThread().getName());
                int[] sequence = this.createSequence();
                while (i != this.n) {
                    sync.notify();
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(sequence[i] + " " + Thread.currentThread().getName());
                    i++;
                }
            }
        });
    }

    private Runnable createRunnable1() {
        return () -> {
            synchronized (sync) {
                System.out.println("Start " + Thread.currentThread().getName());
                int[] sequence = this.createSequence();
                while (i != this.n) {
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(sequence[i] + " " + Thread.currentThread().getName());
                    i++;
                    sync.notify();
                }
            }
        };
    }

    private Runnable createRunnable2() {
        return () -> {
            synchronized (sync) {
                System.out.println("Start " + Thread.currentThread().getName());
                int[] sequence = this.createSequence();
                while (i != this.n) {
                    sync.notify();
                    try {
                        sync.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println(sequence[i] + " " + Thread.currentThread().getName());
                    i++;
                }
            }
        };
    }

    public Thread[] getThreads() {
        return new Thread[]{createThread1(), createThread2()};
    }

    public void startThreads() {
        createThread1().start();
        createThread2().start();
    }

    public void startRunnable() {
        Executor exe1 = Executors.newSingleThreadExecutor();
        Executor exe2 = Executors.newSingleThreadExecutor();
        exe1.execute(createRunnable1());
        exe2.execute(createRunnable2());
    }
}
