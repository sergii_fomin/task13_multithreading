package com.fomin.model;

import java.time.LocalTime;
import java.util.concurrent.TimeUnit;

public class CriticalSections {
    private Object sync1;
    private Object sync2;
    private Object sync3;

    public CriticalSections() {
        sync1 = new Object();
        sync2 = new Object();
        sync3 = new Object();
    }

    private Thread method1() {
        return new Thread(() -> {
            synchronized (sync1){
                try {
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Method 1 done." + LocalTime.now());
            }
        });
    }

    private Thread method2() {
        return new Thread(() -> {
            synchronized (sync2){
                try {
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Method 2 done." + LocalTime.now());
            }
        });
    }

    private Thread method3() {
        return new Thread(() -> {
            synchronized (sync3){
                try {
                    TimeUnit.SECONDS.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("Method 3 done." + LocalTime.now());
            }
        });
    }

    public void startMethods() {
        method1().start();
        method2().start();
        method3().start();
    }
}
