package com.fomin.model;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.util.Scanner;

public class User1 {

    private PipedInputStream in;
    private PipedOutputStream out;
    private Scanner scann;
    private final Object sync;

    public User1(PipedInputStream in, PipedOutputStream out, Object sync) {
        this.in = in;
        this.out = out;
        this.sync = sync;
        scann = new Scanner(System.in);
    }

    public void startThreads() {
        outputPipe().start();
        inputPipe().start();
    }

    private Thread outputPipe() {
        return new Thread(() -> {
            synchronized (sync) {
                try {
                    sync.notify();
                    System.out.println("User1 enter: ");
                    out.write(scann.nextLine().getBytes());
                    sync.wait();
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private Thread inputPipe() {
        return new Thread(() -> {
            synchronized (sync) {
                try {
                    sync.notify();
                    sync.wait();
                    int data = in.read();
                    while (data != -1) {
                        System.out.print((char) data);
                        data = in.read();
                    }
                } catch (IOException | InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
