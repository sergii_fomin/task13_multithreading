package com.fomin.controller;

import com.fomin.model.Fibonacci;
import com.fomin.model.PingPong;
import com.fomin.model.Sleep;
import com.fomin.model.FibonacciCallable;
import com.fomin.model.CriticalSection;
import com.fomin.model.CriticalSections;

import java.util.concurrent.ExecutionException;

public class Controller {
    public void printPingPong(){
        PingPong pingPong = new PingPong();
        pingPong.playRunnable();
    }

    public void fibonacci() {
        Fibonacci fibonacci = new Fibonacci(5);
        fibonacci.startRunnable();
    }

    public void sumFibonacci() {
        FibonacciCallable fibonacciCallable1 = new FibonacciCallable(5);
        FibonacciCallable fibonacciCallable2 = new FibonacciCallable(10);
        FibonacciCallable fibonacciCallable3 = new FibonacciCallable(15);
        FibonacciCallable fibonacciCallable4 = new FibonacciCallable(20);

        try{
            fibonacciCallable1.getSum();
            fibonacciCallable2.getSum();
            fibonacciCallable3.getSum();
            fibonacciCallable4.getSum();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void sleepThread() {
        Sleep sleepThreads1 = new Sleep(3);
        Sleep sleepThreads2 = new Sleep(5);
        sleepThreads1.createSleepingThreads();
        sleepThreads2.createSleepingThreads();
    }

    public void invoke() {
        CriticalSection criticalSection = new CriticalSection();
        criticalSection.startMethods();
        CriticalSections criticalSections = new CriticalSections();
        criticalSections.startMethods();
    }
}
