package com.fomin;

import com.fomin.view.View;

public class App {

    public static void main(String[] args) {
        View view = new View();
        view.showMenu();
    }
}
